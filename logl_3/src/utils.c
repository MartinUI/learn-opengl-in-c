#include <GLFW/glfw3.h>
#include <stdbool.h>
#include <stdio.h>

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

bool parse_file_into_str( const char *file_name, char *shader_str, int max_len ) {
	FILE *file = fopen( file_name, "r" );
	if ( !file ) {
		printf( "ERROR: opening file for reading: %s\n", file_name );
		return false;
	}
	size_t cnt = fread( shader_str, 1, max_len - 1, file );
	if ( (int)cnt >= max_len - 1 ) {
		printf( "WARNING: file %s too big - truncated.\n", file_name );
	}
	if ( ferror( file ) ) {
		printf( "ERROR: reading shader file %s\n", file_name );
		fclose( file );
		return false;
	}
	// append \0 to end of file string
	shader_str[cnt] = 0;
	fclose( file );
	return true;
}

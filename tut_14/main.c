#include <stdio.h>	
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define NK_IMPLEMENTATION
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_BUTTON_TRIGGER_ON_RELEASE
#include "nuklear.h"

#define NK_GLFW_GL3_IMPLEMENTATION
#include "nuklear_glfw_gl3.h"

#include "mesh.h"
#include "shader.h"
#include "utils.h"

#include "stretchy_buffer.h"
#include "linmath.h"


// Window dimensions
const GLint WIDTH = 800, HEIGHT = 600;

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

// timing
float deltaTime = 0.0f; // time between current frame and last frame
float lastFrame = 0.0f;

const float toRoradians = 3.14159265f / 180.0f;

unsigned int indices[] = {
	0, 3, 1,
	1, 3, 2,
	2, 3, 0,
	0, 1, 2
};
GLfloat vertices[] = {
	-1.0f, -1.0f, 0.0f,
	0.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 0.0f,
	0.0f, 1.0f, 0.0f
};

unsigned int cube_indices[] = {
	 	// front
		0, 1, 2,
		2, 3, 0,
		// right
		1, 5, 6,
		6, 2, 1,
		// back
		7, 6, 5,
		5, 4, 7,
		// left
		4, 0, 3,
		3, 7, 4,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// top
		3, 2, 6,
		6, 7, 3};

GLfloat cube_vertices[] = {
	// front
    -1.0, -1.0,  1.0,
     1.0, -1.0,  1.0,
     1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,
    // back
    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0,  1.0, -1.0,
    -1.0,  1.0, -1.0
};

int main()
{
	// Initialise GLFW
	if (!glfwInit())
	{
		printf("GLFW initialisation failed!");
		glfwTerminate();
		return 1;
	}

	struct nk_context* nk;
    struct nk_font_atlas* atlas;
	// Setup GLFW window properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Core Profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Allow Forward Compatbility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Create the window
	GLFWwindow *mainWindow = glfwCreateWindow(WIDTH, HEIGHT, "Demo", NULL, NULL);
	if (!mainWindow)
	{
		printf("GLFW window creation failed!");
		glfwTerminate();
		return 1;
	}

	// Get Buffer Size information
	int bufferWidth, bufferHeight;
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);


	double xpos, ypos;
	
	// Set context for GLEW to use
	glfwMakeContextCurrent(mainWindow);
	glfwSetFramebufferSizeCallback(mainWindow, framebuffer_size_callback);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	firstMouse = true;
	yaw = -90.0f; // yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
	pitch = 0.0f;
	lastX = WIDTH / 2.0;
	lastY = HEIGHT / 2.0;
	xoffset = 0.0f;
	yoffset = 0.0f;
	sensitivity = 0.01f; // change this value to your liking

	cameraPos[0] = 0.0f;
	cameraPos[1] = 0.0f;
	cameraPos[2] = 3.0f;

	cameraFront[0] = 0.0f;
	cameraFront[1] = 0.0f;
	cameraFront[2] = -1.0f;

	cameraUp[0] = 0.0f;
	cameraUp[1] = 1.0f;
	cameraUp[2] = 0.0f;

	glfwSetCursorPosCallback(mainWindow, mouse_callback);

	fov = 45.0f;

	glfwSetScrollCallback(mainWindow, scroll_callback);

	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		printf("GLEW initialisation failed!");
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}
	glEnable(GL_DEPTH_TEST);
	// Setup Viewport size
	glViewport(0, 0, bufferWidth, bufferHeight);
	//Meshes
	mesh *scene_meshes = NULL;
	//Mesh object 1
	mesh obj1 = MESH_OBJ_INIT;
	//CreateMesh(&obj1, vertices, indices, 12, 12);
	CreateMesh(&obj1, cube_vertices, cube_indices, 24, 36);
	sb_push(scene_meshes, obj1);
	//Mesh object 2
	mesh obj2 = MESH_OBJ_INIT;
	CreateMesh(&obj2, cube_vertices, cube_indices, 24, 36);
	sb_push(scene_meshes, obj2);

	shader *scene_shaders = NULL;
	//Shaders
	shader sh1 = SHADER_OBJ_INIT;
	CreateShader(&sh1, "shaders/test_vs.glsl", "shaders/test_fs.glsl");
	sb_push(scene_shaders, sh1);
	//Light shader
	shader sh2 = SHADER_OBJ_INIT;
	CreateShader(&sh2, "shaders/test_vs.glsl", "shaders/light_fs.glsl");
	sb_push(scene_shaders, sh2);

	//Projection matrix
	mat4x4 projection;
	mat4x4_identity(projection);
	mat4x4_perspective(projection, fov, (float)WIDTH / (float)HEIGHT, 1.0f, 100.0f);

	nk = nk_glfw3_init(mainWindow, NK_GLFW3_INSTALL_CALLBACKS);
    nk_glfw3_font_stash_begin(&atlas);
    nk_glfw3_font_stash_end();
	// Loop until window closed
	while (!glfwWindowShouldClose(mainWindow))
	{
		// Get + Handle user input events
		glfwPollEvents();
		glfwGetCursorPos(mainWindow, &xpos, &ypos);
		//Delta time
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		nk_glfw3_new_frame();
		if (nk_begin(nk, "Camera properties", nk_rect(20, 20, 230, 180),
            NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
            NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
        {
            nk_layout_row_dynamic(nk, 12, 1);
			nk_labelf(nk, NK_TEXT_LEFT, "Progressbar: %f" , fov);
			nk_labelf(nk, NK_TEXT_LEFT, "First mouse: %d" , firstMouse);
			nk_labelf(nk, NK_TEXT_LEFT, "X pos: %f" , xpos);
			nk_labelf(nk, NK_TEXT_LEFT, "Y pos: %f" , ypos);
			nk_labelf(nk, NK_TEXT_LEFT, "X offset: %f" , xoffset);
			nk_labelf(nk, NK_TEXT_LEFT, "Y offset: %f" , yoffset);
			nk_labelf(nk, NK_TEXT_LEFT, "Yaw: %f" , yaw);
			nk_labelf(nk, NK_TEXT_LEFT, "Pitch: %f" , pitch);
            
        }
        nk_end(nk);

		processInput(mainWindow, deltaTime, cameraPos, cameraFront, cameraUp);

		// Clear window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		nk_glfw3_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);

		glUseProgram(scene_shaders[0].shaderID);

		//Model matrix

		mat4x4 model;
		mat4x4_identity(model);
		mat4x4_translate(model, 0.0f, 0.0f, 0.0f);
		mat4x4_scale_aniso(model, model, 0.4f, 0.4f, 0.4f);

		// camera/view transformation
		mat4x4 view;
		vec3 cameraNewPos;
		vec3_add(cameraNewPos, cameraPos, cameraFront);
		mat4x4_look_at(view, cameraPos, cameraNewPos, cameraUp);

		vec3 objectColor = {1.0f, 0.5f, 0.31f};
		vec3 lightColor = {1.0f, 1.0f, 1.0f};

		//Attach model matrix
		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);
		//Attach view matrix
		glUniformMatrix4fv(scene_shaders[0].uniformView, 1, GL_FALSE, view[0]);
		//Attach projection matrix
		glUniformMatrix4fv(scene_shaders[0].uniformProjection, 1, GL_FALSE, projection[0]);
		//Attach object color vector
		glUniform3fv(scene_shaders[0].objectColor, 1, &objectColor[0]);
		//Attach light color vector
		glUniform3fv(scene_shaders[0].lightColor, 1, &lightColor[0]);
		//Render mesh
		RenderMesh(&scene_meshes[0]);

		glUseProgram(scene_shaders[1].shaderID);

		mat4x4_identity(model);

		vec3 lightPos = {1.2f, 1.0f, 2.0f};

		mat4x4_translate(model, lightPos[0], lightPos[1], lightPos[2]);
		mat4x4_scale_aniso(model, model, 0.2f, 0.2f, 0.2f);

		glUniformMatrix4fv(scene_shaders[1].uniformModel, 1, GL_FALSE, model[0]);
		//Attach view matrix
		glUniformMatrix4fv(scene_shaders[1].uniformView, 1, GL_FALSE, view[0]);
		//Attach projection matrix
		glUniformMatrix4fv(scene_shaders[1].uniformProjection, 1, GL_FALSE, projection[0]);
		//Attach model matrix
		glUniformMatrix4fv(scene_shaders[1].uniformModel, 1, GL_FALSE, model[0]);

		RenderMesh(&scene_meshes[1]);

		glUseProgram(0);

		glfwSwapBuffers(mainWindow);
	}

	sb_free(scene_meshes);
	sb_free(scene_shaders);
	nk_glfw3_shutdown();
    glfwTerminate();
	return 0;
}

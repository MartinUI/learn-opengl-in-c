#version 330

struct Texture {
    sampler2D diffuse;
    sampler2D specular;  
    float shininess;
};

struct Light {
    vec3 position;
    //Spot
    vec3 direction;
    float cutOff;
    float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    //Attenuation
    vec3 attenuation;
};

uniform Texture tex;
uniform Light light;    
uniform vec3 viewPos;

in vec2 texCoord;
in vec3 normal;
in vec3 fragPos; 

out vec4 colour;

void main()
{
    // check if lighting is inside the spotlight cone
    //vec3 att = light.attenuation;
    vec3 fill = vec3(0.2);
    vec3 ambient = light.ambient * texture(tex.diffuse, texCoord).rgb;

    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(light.position - fragPos);  

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(tex.diffuse, texCoord).rgb;

    vec3 viewDir = normalize(viewPos - fragPos);
    vec3 reflectDir = reflect(-lightDir, norm);

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), tex.shininess);
    vec3 specular = light.specular * spec * texture(tex.specular, texCoord).rgb;   

    float theta = dot(lightDir, normalize(-light.direction));    
    float epsilon   = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

    diffuse  *= intensity;
    specular *= intensity;

    float dis = length(light.position - fragPos);
    float attenuation = 1.0 / (light.attenuation.x + light.attenuation.y * dis + light.attenuation.z * (dis * dis));     

    ambient  *= attenuation;  
    diffuse  *= attenuation;
    specular *= attenuation;

    vec3 result = ambient + diffuse + specular; 
    
    colour = vec4(result, 1.0);

}  
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "mesh.h"
#include "shader.h"
#include "utils.h"

#include "stretchy_buffer.h"
#include "linmath.h"


// Window dimensions
const GLint WIDTH = 800, HEIGHT = 600;

// timing
float deltaTime = 0.0f; // time between current frame and last frame
float lastFrame = 0.0f;

const float toRoradians = 3.14159265f / 180.0f;

unsigned int indices[] = {
	0, 3, 1,
	1, 3, 2,
	2, 3, 0,
	0, 1, 2};
GLfloat vertices[] = {
	-1.0f, -1.0f, 0.0f,
	0.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 0.0f,
	0.0f, 1.0f, 0.0f};

int main()
{
	// Initialise GLFW
	if (!glfwInit())
	{
		printf("GLFW initialisation failed!");
		glfwTerminate();
		return 1;
	}

	// Setup GLFW window properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Core Profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Allow Forward Compatbility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Create the window
	GLFWwindow *mainWindow = glfwCreateWindow(WIDTH, HEIGHT, "Demo", NULL, NULL);
	if (!mainWindow)
	{
		printf("GLFW window creation failed!");
		glfwTerminate();
		return 1;
	}

	// Get Buffer Size information
	int bufferWidth, bufferHeight;
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);

	// Set context for GLEW to use
	glfwMakeContextCurrent(mainWindow);
	glfwSetFramebufferSizeCallback(mainWindow, framebuffer_size_callback);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	firstMouse = true;
	yaw = -90.0f; // yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
	pitch = 0.0f;
	lastX = WIDTH / 2.0;
	lastY = HEIGHT / 2.0;
	xoffset = 0.0f;
	yoffset = 0.0f;
	sensitivity = 0.1f; // change this value to your liking

	cameraPos[0] = 0.0f;
	cameraPos[1] = 0.0f;
	cameraPos[2] = 3.0f;

	cameraFront[0] = 0.0f;
	cameraFront[1] = 0.0f;
	cameraFront[2] = -1.0f;

	cameraUp[0] = 0.0f;
	cameraUp[1] = 1.0f;
	cameraUp[2] = 0.0f;

	glfwSetCursorPosCallback(mainWindow, mouse_callback);

	fov = 45.0f;

	glfwSetScrollCallback(mainWindow, scroll_callback);

	glewExperimental = GL_TRUE;

	struct nk_context *ctx;
	//nk_init_fixed(&ctx, calloc(1, MAX_MEMORY), MAX_MEMORY, &font);

	if (glewInit() != GLEW_OK)
	{
		printf("GLEW initialisation failed!");
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}
	glEnable(GL_DEPTH_TEST);
	// Setup Viewport size
	glViewport(0, 0, bufferWidth, bufferHeight);
	//Meshes
	mesh *scene_meshes = NULL;
	//Mesh object 1
	mesh obj1 = MESH_OBJ_INIT;
	CreateMesh(&obj1, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj1);
	//Mesh object 2
	mesh obj2 = MESH_OBJ_INIT;
	CreateMesh(&obj2, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj2);

	shader *scene_shaders = NULL;
	//Shaders
	shader sh1 = SHADER_OBJ_INIT;
	CreateShader(&sh1, "shaders/test_vs.glsl", "shaders/test_fs.glsl");
	sb_push(scene_shaders, sh1);

	//Projection matrix
	mat4x4 projection;
	mat4x4_identity(projection);
	mat4x4_perspective(projection, fov, (float)WIDTH / (float)HEIGHT, 1.0f, 100.0f);



	// Loop until window closed
	while (!glfwWindowShouldClose(mainWindow))
	{
		// Get + Handle user input events
		glfwPollEvents();
		//Delta time
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;


		processInput(mainWindow, deltaTime, cameraPos, cameraFront, cameraUp);

		// Clear window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(scene_shaders[0].shaderID);

		//Model matrix

		mat4x4 model;
		mat4x4_identity(model);
		mat4x4_translate(model, 0.0f, 0.0f, -2.5f);
		mat4x4_scale_aniso(model, model, 0.4f, 0.4f, 1.0f);

		// camera/view transformation
		mat4x4 view;
		vec3 cameraNewPos;
		vec3_add(cameraNewPos, cameraPos, cameraFront);
		mat4x4_look_at(view, cameraPos, cameraNewPos, cameraUp);

		//Attach model matrix
		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);
		//Attach view matrix
		glUniformMatrix4fv(scene_shaders[0].uniformView, 1, GL_FALSE, view[0]);
		//Attach projection matrix
		glUniformMatrix4fv(scene_shaders[0].uniformProjection, 1, GL_FALSE, projection[0]);
		//Render mesh
		RenderMesh(&scene_meshes[0]);

		mat4x4_identity(model);
		mat4x4_translate(model, 0.0f, 1.0f, -2.5f);
		mat4x4_scale_aniso(model, model, 0.4f, 0.4f, 1.0f);

		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);

		RenderMesh(&scene_meshes[1]);

		glUseProgram(0);

		glfwSwapBuffers(mainWindow);
	}

	sb_free(scene_meshes);
	sb_free(scene_shaders);
	glfwTerminate();
	return 0;
}

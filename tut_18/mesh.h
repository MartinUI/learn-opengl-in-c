#ifndef MESH_H
#define MESH_H

#include <GLFW/glfw3.h>

#define MESH_OBJ_INIT {0, 0, 0, 0, 0, 0}

typedef struct {
    GLuint VBO;
    GLuint VAO;
    GLuint IBO;
    GLuint NBO;
    GLsizei indexCount;
    GLsizei normalsCount;
} mesh;

void CreateMesh(mesh *mesh_object, GLfloat *vertices, unsigned int *indeces, unsigned int *normals,  unsigned int numOfVerteces, unsigned int numOfIndecies, unsigned int numOfNormals);
void RenderMesh(mesh *mesh_object);
void ClearMesh();

#endif
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "material.h"
#include "shader.h"


void AddMaterial(shader *shader_object, material *material_object){
    //Attach object color vector
	glUniform3fv(shader_object->objectColor, 1, material_object->objectColor[0]);
	//Attach light color vector
	glUniform3fv(shader_object->lightColor, 1, material_object->lightColor[0]);
}




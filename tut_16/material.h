#ifndef MATERIAL_H
#define MATERIAL_H

#include "linmath.h"
#include "shader.h"

//#define MATERIAL_OBJ_INIT {NULL, NULL}

typedef struct{
    vec3 *objectColor;
    vec3 *lightColor;
} material;

void AddMaterial(shader *shader_object, material *material_object);

#endif
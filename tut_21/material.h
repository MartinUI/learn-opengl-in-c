#ifndef MATERIAL_H
#define MATERIAL_H

#include <GLFW/glfw3.h>

#include "linmath.h"
#include "shader.h"

#define COL_WHITE {1.0f, 1.0f, 1.0f}

typedef struct{
    vec3 *materialAmbient;
    vec3 *materialDiffuse;
    vec3 *materialSpecular;
    GLfloat materialShininess;
    vec3 *lightAmbient;
    vec3 *lihtDiffuse;
    vec3 *lightSpecular;
} Phong;

typedef struct{
    vec3 *materialSpecular;
    GLfloat materialShininess;
    vec3 *lightAmbient;
    vec3 *lihtDiffuse;
    vec3 *lightSpecular;
} TexPhong;


void SetPhongMaterial(shader *shader_object, Phong *material_object, vec3 *lightDirection /* vec3 *lightPosition */);
void SetTexPhongMaterial(shader *shader_object, TexPhong *material_object, vec3 *lightDirection /* vec3 *lightPosition */);

#endif


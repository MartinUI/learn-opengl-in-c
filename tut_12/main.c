#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define NK_IMPLEMENTATION
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_BUTTON_TRIGGER_ON_RELEASE
#include "nuklear.h"

#define NK_GLFW_GL3_IMPLEMENTATION
#include "nuklear_glfw_gl3.h"

#include "stretchy_buffer.h"
#include "linmath.h"

#include "mesh.h"
#include "shader.h"

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

// Window dimensions
const GLint WIDTH = 800, HEIGHT = 600;

vec3 cameraPos = {0.0f, 0.0f, 3.0f};
vec3 cameraFront = {0.0f, 0.0f, -1.0f};
vec3 cameraUp = {0.0f, 1.0f, 0.0f};

// timing
float deltaTime = 0.0f; // time between current frame and last frame
float lastFrame = 0.0f;

const float toRoradians = 3.14159265f / 180.0f;

unsigned int indices[] = {
	0, 3, 1,
	1, 3, 2,
	2, 3, 0,
	0, 1, 2};
GLfloat vertices[] = {
	-1.0f, -1.0f, 0.0f,
	0.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 0.0f,
	0.0f, 1.0f, 0.0f};


void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	float cameraSpeed = 2.5f * deltaTime;

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		vec3 cameraPosMul;
		vec3_scale(cameraPosMul, cameraPos, cameraSpeed);
		vec3_add(cameraPos, cameraPos, cameraPosMul);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		vec3 cameraPosMul;
		vec3_scale(cameraPosMul, cameraPos, cameraSpeed);
		vec3_sub(cameraPos, cameraPos, cameraPosMul);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		vec3 cameraPosMul;
		vec3 cameraCross;
		vec3_mul_cross(cameraCross, cameraFront, cameraUp);
		vec3_norm(cameraCross, cameraCross);
		vec3_scale(cameraPosMul, cameraCross, cameraSpeed);
		vec3_sub(cameraPos, cameraPos, cameraPosMul);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		vec3 cameraPosMul;
		vec3 cameraCross;
		vec3_mul_cross(cameraCross, cameraFront, cameraUp);
		vec3_norm(cameraCross, cameraCross);
		vec3_scale(cameraPosMul, cameraCross, cameraSpeed);
		vec3_add(cameraPos, cameraPos, cameraPosMul);
	}
}

int main()
{
	// Initialise GLFW
	if (!glfwInit())
	{
		printf("GLFW initialisation failed!");
		glfwTerminate();
		return 1;
	}
	struct nk_context* nk;
	struct nk_colorf bg;
    struct nk_font_atlas* atlas;
	
	// Setup GLFW window properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Core Profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Allow Forward Compatbility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Create the window
	GLFWwindow *mainWindow = glfwCreateWindow(WIDTH, HEIGHT, "Demo", NULL, NULL);
	if (!mainWindow)
	{
		printf("GLFW window creation failed!");
		glfwTerminate();
		return 1;
	}

	// Get Buffer Size information
	int bufferWidth, bufferHeight;
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);

	// Set context for GLEW to use
	glfwMakeContextCurrent(mainWindow);

	// Allow modern extension features
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		printf("GLEW initialisation failed!");
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}
	glEnable(GL_DEPTH_TEST);
	// Setup Viewport size
	glViewport(0, 0, bufferWidth, bufferHeight);
	//Meshes
	mesh *scene_meshes = NULL;
	//Mesh object 1
	mesh obj1 = MESH_OBJ_INIT;
	CreateMesh(&obj1, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj1);
	//Mesh object 2
	mesh obj2 = MESH_OBJ_INIT;
	CreateMesh(&obj2, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj2);

	shader *scene_shaders = NULL;
	//Shaders
	shader sh1 = SHADER_OBJ_INIT;
	CreateShader(&sh1, "shaders/test_vs.glsl", "shaders/test_fs.glsl");
	sb_push(scene_shaders, sh1);

	//Projection matrix
	mat4x4 projection;
	mat4x4_identity(projection);
	mat4x4_perspective(projection, 45.0f, (float)WIDTH / (float)HEIGHT, 1.0f, 100.0f);

	nk = nk_glfw3_init(mainWindow, NK_GLFW3_INSTALL_CALLBACKS);
    nk_glfw3_font_stash_begin(&atlas);
    nk_glfw3_font_stash_end();
	
	bg.r = 0.0f, bg.g = 0.0f, bg.b = 0.0f, bg.a = 1.0f;
	while (!glfwWindowShouldClose(mainWindow))
	{
		// Get + Handle user input events
		glfwPollEvents();

		nk_glfw3_new_frame();
		
		/* GUI */
        if (nk_begin(nk, "Demo", nk_rect(50, 50, 230, 250),
            NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
            NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
        {
            enum {EASY, HARD};
            static int op = EASY;
            static int property = 20;
            nk_layout_row_static(nk, 30, 80, 1);
            if (nk_button_label(nk, "button"))
                fprintf(stdout, "button pressed\n");

            nk_layout_row_dynamic(nk, 30, 2);
            if (nk_option_label(nk, "easy", op == EASY)) op = EASY;
            if (nk_option_label(nk, "hard", op == HARD)) op = HARD;

            nk_layout_row_dynamic(nk, 25, 1);
            nk_property_int(nk, "Compression:", 0, &property, 100, 10, 1);

            nk_layout_row_dynamic(nk, 20, 1);
            nk_label(nk, "background:", NK_TEXT_LEFT);
            nk_layout_row_dynamic(nk, 25, 1);
            if (nk_combo_begin_color(nk, nk_rgb_cf(bg), nk_vec2(nk_widget_width(nk),400))) {
                nk_layout_row_dynamic(nk, 120, 1);
                bg = nk_color_picker(nk, bg, NK_RGBA);
                nk_layout_row_dynamic(nk, 25, 1);
                bg.r = nk_propertyf(nk, "#R:", 0, bg.r, 1.0f, 0.01f,0.005f);
                bg.g = nk_propertyf(nk, "#G:", 0, bg.g, 1.0f, 0.01f,0.005f);
                bg.b = nk_propertyf(nk, "#B:", 0, bg.b, 1.0f, 0.01f,0.005f);
                bg.a = nk_propertyf(nk, "#A:", 0, bg.a, 1.0f, 0.01f,0.005f);
                nk_combo_end(nk);
            }
        }
        nk_end(nk);

		//Delta time
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		processInput(mainWindow);

		// Clear window
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(bg.r, bg.g, bg.b, bg.a);
		nk_glfw3_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);

		glUseProgram(scene_shaders[0].shaderID);

		//Model matrix

		mat4x4 model;
		mat4x4_identity(model);
		mat4x4_translate(model, 0.0f, 0.0f, -2.5f);
		mat4x4_scale_aniso(model, model, 0.4f, 0.4f, 1.0f);

		// camera/view transformation
		mat4x4 view;
		//mat4x4_identity(view);
		/* float radius = 10.0f;
        float camX   = sin(glfwGetTime()) * radius;
        float camZ   = cos(glfwGetTime()) * radius;

		vec3 eye = {camX, 0.0f, camZ};
		vec3 center = {0.0f, 0.0f, 0.0f};
		vec3 up = {0.0f, 1.0f, 0.0f}; */
		vec3 cameraNewPos;
		vec3_add(cameraNewPos, cameraPos, cameraFront);
		mat4x4_look_at(view, cameraPos, cameraNewPos, cameraUp);

		//Attach model matrix
		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);
		//Attach view matrix
		glUniformMatrix4fv(scene_shaders[0].uniformView, 1, GL_FALSE, view[0]);
		//Attach projection matrix
		glUniformMatrix4fv(scene_shaders[0].uniformProjection, 1, GL_FALSE, projection[0]);
		//Render mesh
		RenderMesh(&scene_meshes[0]);

		mat4x4_identity(model);
		mat4x4_translate(model, 0.0f, 1.0f, -2.5f);
		mat4x4_scale_aniso(model, model, 0.4f, 0.4f, 1.0f);

		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);

		RenderMesh(&scene_meshes[1]);

		glUseProgram(0);

        
		glfwSwapBuffers(mainWindow);
	}
	sb_free(scene_meshes);
	sb_free(scene_shaders);
	nk_glfw3_shutdown();
    glfwTerminate();
	return 0;
}

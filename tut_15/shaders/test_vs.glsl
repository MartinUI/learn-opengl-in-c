#version 330

layout (location = 0) in vec3 pos;
layout (location = 0) in vec3 normals;
//out vec4 vCol;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 normal;
out vec3 fragPos;

void main()
{    
    fragPos = vec3(model * vec4(pos, 1.0));
    normal = mat3(transpose(inverse(model))) * normals;

    gl_Position = projection * view * model * vec4(pos, 1.0);
	//vCol = vec4(clamp(pos, 0.0f, 1.0f),1.0f);
}
#ifndef UTILS_H
#define UTILS_H

#include <GLFW/glfw3.h>

void processInput(GLFWwindow *);
void framebuffer_size_callback(GLFWwindow* , int, int);
bool parse_file_into_str( const char *, char *, int);

#endif
#ifndef MESH_H
#define MESH_H

#include <GLFW/glfw3.h>

#define MESH_OBJ_INIT {0, 0, 0}

typedef struct {
    unsigned int VBO;
    unsigned int cubeVAO;
    unsigned int lightVAO;
} mesh;

void CreateMesh(mesh *mesh_object, float *vertices);


/* void RenderMesh(mesh *mesh_object);
void ClearMesh();
 */
#endif
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "mesh.h"

void CreateMesh(mesh *mesh_object, float *vertices){

    // mesh_object->indexCount = numOfIndecies;
	// mesh_object->normalsCount = numOfNormals;

    //unsigned int VBO, cubeVAO;
    glGenVertexArrays(1, &(mesh_object->cubeVAO));
    glGenBuffers(1, &(mesh_object->VBO));

    glBindBuffer(GL_ARRAY_BUFFER, mesh_object->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(mesh_object->cubeVAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    // second, configure the light's VAO (VBO stays the same; the vertices are the same for the light object which is also a 3D cube)
    //unsigned int lightVAO;
    glGenVertexArrays(1, &(mesh_object->lightVAO));
    glBindVertexArray(mesh_object->lightVAO);

    glBindBuffer(GL_ARRAY_BUFFER, mesh_object->VBO);
    // note that we update the lamp's position attribute's stride to reflect the updated buffer data
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
}




/*  void RenderMesh(mesh *mesh_object){ 
	//Bind to vertex array
	glBindVertexArray(mesh_object->cubeVAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void ClearMesh(mesh *mesh_object){    
    if (mesh_object->VBO != 0) {
        glDeleteBuffers(1, &(mesh_object->VBO));
        mesh_object->VBO = 0;
    }
    if (mesh_object->cubeVAO != 0) {
        glDeleteVertexArrays(1, &(mesh_object->cubeVAO));
        mesh_object->cubeVAO = 0;
    }
	if (mesh_object->lightVAO != 0) {
        glDeleteVertexArrays(1, &(mesh_object->lightVAO));
        mesh_object->lightVAO = 0;
    }
} */

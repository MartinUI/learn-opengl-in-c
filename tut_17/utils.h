#ifndef UTILS_H
#define UTILS_H

#include <GLFW/glfw3.h>
#include "linmath.h"

vec3 cameraPos;
vec3 cameraFront;
vec3 cameraUp;

bool firstMouse;
float yaw;
float pitch;
float lastX;
float lastY;
float xoffset;
float yoffset;
float sensitivity;

float fov;

void processInput(GLFWwindow *window, float deltaTime, vec3 cameraPos, vec3 cameraFront, vec3 cameraUp);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

#endif
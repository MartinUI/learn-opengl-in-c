#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "material.h"
#include "shader.h"


void SetPhongMaterial(shader *shader_object, Phong *material_object){

	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "objectColor"), 1, material_object->objectColor[0]); 
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "lightColor"), 1, material_object->lightColor[0]); 
	
}




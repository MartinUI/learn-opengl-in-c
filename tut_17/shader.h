#ifndef SHADER_H
#define SHADER_H

#include <GLFW/glfw3.h>

#include "stdbool.h"

#define SHADER_OBJ_INIT {0, 0, 0, 0, 1.0f, 1.0f}

typedef struct{
    GLuint shaderID;
    GLuint uniformModel;
    GLuint uniformView;
    GLuint uniformProjection;
    GLfloat lightPos;
    GLfloat viewPos;
} shader;

char shader_buf[1024 * 256];
const GLchar *shader_source_pointer;

static bool ParseFileIntoStr(const char *file_name, char *shader_str, int max_len);
static void AddShader(GLuint theProgram, char *shaderCode, GLenum shaderType);
void CreateShader(shader *shader_object, char *vertexShaderCode, char *fragmentShaderCode);


#endif
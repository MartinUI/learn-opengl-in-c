#ifndef MATERIAL_H
#define MATERIAL_H

#include "linmath.h"
#include "shader.h"

//#define MATERIAL_OBJ_INIT {NULL, NULL}

typedef struct{
    vec3 *objectColor;
    vec3 *lightColor;
} Phong;

void SetPhongMaterial(shader *shader_object, Phong *material_object);

#endif
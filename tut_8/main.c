#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cglm/cglm.h>

#include "mesh.h"
#include "stretchy_buffer.h"

// Window dimensions
const GLint WIDTH = 800, HEIGHT = 600;
const float toRoradians = 3.14159265f / 180.0f;

GLuint shader, uniformModel, uniformProjection;

unsigned int indices[] = {
	0, 3, 1,
	1, 3, 2,
	2, 3, 0,
	0, 1, 2};
GLfloat vertices[] = {
	-1.0f, -1.0f, 0.0f,
	0.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 0.0f,
	0.0f, 1.0f, 0.0f};

bool direction = true;
float triOffset = 0.0f;
float triMaxOffset = 0.7f;
float triIncrement = 0.005f;
float curAngle = 0.0f;

bool sizeDirection = true;
float curSize = 0.4f;
float maxSize = 0.8f;
float minSize = 0.1f;

char shader_buf[1024 * 256];
const GLchar *shader_source_pointer;

bool parse_file_into_str(const char *file_name, char *shader_str, int max_len)
{
	FILE *file = fopen(file_name, "r");
	if (!file)
	{
		printf("ERROR: opening file for reading: %s\n", file_name);
		return false;
	}
	size_t cnt = fread(shader_str, 1, max_len - 1, file);
	if ((int)cnt >= max_len - 1)
	{
		printf("WARNING: file %s too big - truncated.\n", file_name);
	}
	if (ferror(file))
	{
		printf("ERROR: reading shader file %s\n", file_name);
		fclose(file);
		return false;
	}
	// append \0 to end of file string
	shader_str[cnt] = 0;
	fclose(file);
	return true;
}

void AddShader(GLuint theProgram, char *shaderCode, GLenum shaderType)
{

	GLuint theShader = glCreateShader(shaderType);

	parse_file_into_str(shaderCode, shader_buf, 1024 * 256);
	shader_source_pointer = (const GLchar *)shader_buf;

	glShaderSource(theShader, 1, &shader_source_pointer, NULL);
	glCompileShader(theShader);

	GLint result = 0;
	GLchar eLog[1024] = {0};

	glGetShaderiv(theShader, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(theShader, 1024, NULL, eLog);
		fprintf(stderr, "Error compiling the %d shader: '%s'\n", shaderType, eLog);
		return;
	}

	glAttachShader(theProgram, theShader);
}

void CompileShaders()
{
	shader = glCreateProgram();

	if (!shader)
	{
		printf("Failed to create shader\n");
		return;
	}

	AddShader(shader, "test_vs.glsl", GL_VERTEX_SHADER);
	AddShader(shader, "test_fs.glsl", GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar eLog[1024] = {0};

	glLinkProgram(shader);
	glGetProgramiv(shader, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shader, sizeof(eLog), NULL, eLog);
		printf("Error linking program: '%s'\n", eLog);
		return;
	}

	glValidateProgram(shader);
	glGetProgramiv(shader, GL_VALIDATE_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shader, sizeof(eLog), NULL, eLog);
		printf("Error validating program: '%s'\n", eLog);
		return;
	}
	uniformModel = glGetUniformLocation(shader, "model");
	uniformProjection = glGetUniformLocation(shader, "projection");
}

int main()
{
	// Initialise GLFW
	if (!glfwInit())
	{
		printf("GLFW initialisation failed!");
		glfwTerminate();
		return 1;
	}

	// Setup GLFW window properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Core Profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Allow Forward Compatbility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Create the window
	GLFWwindow *mainWindow = glfwCreateWindow(WIDTH, HEIGHT, "Test Window", NULL, NULL);
	if (!mainWindow)
	{
		printf("GLFW window creation failed!");
		glfwTerminate();
		return 1;
	}

	// Get Buffer Size information
	int bufferWidth, bufferHeight;
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);

	// Set context for GLEW to use
	glfwMakeContextCurrent(mainWindow);

	// Allow modern extension features
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		printf("GLEW initialisation failed!");
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}
	glEnable(GL_DEPTH_TEST);
	// Setup Viewport size
	glViewport(0, 0, bufferWidth, bufferHeight);

	//Double pointer meshes
	/* mesh **scene_meshes = NULL;	
	mesh *obj1 = malloc(sizeof(mesh *));
	obj1->VAO = 0;
	obj1->VBO = 0;
	obj1->IBO = 0;
	obj1->indexCount = 0;	
	CreateMesh(obj1, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj1); */

	mesh *scene_meshes = NULL;

	mesh obj1 = MESH_INIT;
	CreateMesh(&obj1, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj1);
	mesh obj2 = MESH_INIT;
	CreateMesh(&obj2, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj2);

	CompileShaders();

	//Projection matrix
	mat4 projection = GLM_MAT4_IDENTITY_INIT;
	glm_perspective(45.0f, ((GLfloat)bufferWidth / (GLfloat)bufferHeight), 0.1f, 100.0f, projection);

	// Loop until window closed
	while (!glfwWindowShouldClose(mainWindow))
	{
		// Get + Handle user input events
		glfwPollEvents();

		if (direction)
		{
			triOffset += triIncrement;
		}
		else
		{
			triOffset -= triIncrement;
		}

		if (abs(triOffset) >= triMaxOffset)
		{
			direction = !direction;
		}
		//Translation rotation scale animation
		curAngle += 0.1f;
		if (curAngle >= 360)
		{
			curAngle -= 360;
		}

		if (sizeDirection)
		{
			curSize += 0.001f;
		}
		else
		{
			curSize -= 0.001f;
		}

		if (curSize >= maxSize || curSize <= minSize)
		{
			sizeDirection = !sizeDirection;
		}

		// Clear window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(shader);

		//Model matrix
		mat4 model = GLM_MAT4_IDENTITY_INIT;
		glm_translate(model, (vec3){triOffset, 0.0f, -2.5f});
		//glm_rotate(model,curAngle * toRoradians, (vec3){0.0f,1.0f,0.0f});
		glm_scale(model, (vec3){0.4f, 0.4f, 1.0f});
		//Attach model matrix
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, model[0]);
		//Attach projection matrix
		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, projection[0]);
		//Render mesh
		//RenderMesh(scene_meshes[0]);
		RenderMesh(&scene_meshes[0]);

		glm_mat4_identity(model);		

		glm_translate(model, (vec3){-triOffset, 1.0f, -2.5f});
		glm_scale(model, (vec3){0.4f, 0.4f, 1.0f});
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, model[0]);
		RenderMesh(&scene_meshes[1]);

		glUseProgram(0);

		glfwSwapBuffers(mainWindow);
	}
	sb_free(scene_meshes);
	//free(obj1);
	return 0;
}

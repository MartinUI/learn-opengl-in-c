#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

//#include "mesh.h"
#include "shader.h"
#include "utils.h"
#include "material.h"

#include "stretchy_buffer.h"
#include "linmath.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// Window dimensions
const GLint WIDTH = 800, HEIGHT = 600;

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

// timing
float deltaTime = 0.0f; // time between current frame and last frame
float lastFrame = 0.0f;

const float toRoradians = 3.14159265f / 180.0f;

float vertices[] = {
	// positions          // normals           // texture coords
	-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
	0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
	0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
	0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
	-0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,

	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	-0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

	-0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	-0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
	-0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	-0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

	0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
	0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
	0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	-0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,

	-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
	0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
	0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
	-0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
	-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};

vec3 objectColor = {1.0f, 0.5f, 0.31f};
vec3 lightColor = {1.0f, 1.0f, 1.0f};
vec3 lightPos = {1.0f, 1.0f, 1.0f};

int main()
{
	// Initialise GLFW
	if (!glfwInit())
	{
		printf("GLFW initialisation failed!");
		glfwTerminate();
		return 1;
	}
	// Setup GLFW window properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Core Profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Allow Forward Compatbility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	//Deepth test
	glfwWindowHint(GLFW_DEPTH_BITS, GL_TRUE);

	// Create the window
	GLFWwindow *mainWindow = glfwCreateWindow(WIDTH, HEIGHT, "Demo", NULL, NULL);
	if (!mainWindow)
	{
		printf("GLFW window creation failed!");
		glfwTerminate();
		return 1;
	}

	// Get Buffer Size information
	int bufferWidth, bufferHeight;
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);

	double xpos, ypos;

	// Set context for GLEW to use
	glfwMakeContextCurrent(mainWindow);
	glfwSetFramebufferSizeCallback(mainWindow, framebuffer_size_callback);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	firstMouse = true;
	yaw = -90.0f; // yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
	pitch = 0.0f;
	lastX = WIDTH / 2.0;
	lastY = HEIGHT / 2.0;
	xoffset = 0.0f;
	yoffset = 0.0f;
	sensitivity = 0.01f; // change this value to your liking

	cameraPos[0] = 0.0f;
	cameraPos[1] = 0.0f;
	cameraPos[2] = 3.0f;

	cameraFront[0] = 0.0f;
	cameraFront[1] = 0.0f;
	cameraFront[2] = -1.0f;

	cameraUp[0] = 0.0f;
	cameraUp[1] = 1.0f;
	cameraUp[2] = 0.0f;

	glfwSetCursorPosCallback(mainWindow, mouse_callback);

	fov = 45.0f;

	glfwSetScrollCallback(mainWindow, scroll_callback);

	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		printf("GLEW initialisation failed!");
		glfwDestroyWindow(mainWindow);
		glfwTerminate();

		return 1;
	}
	// Setup Viewport size
	glViewport(0, 0, bufferWidth, bufferHeight);
	glEnable(GL_DEPTH_TEST);
	//Meshes
	//mesh *scene_meshes = NULL;
	//Mesh object 1

	//mesh obj1 = MESH_OBJ_INIT;
	//CreateMesh(&obj1, vertices, indices, 12, 12);

	//Inline
	unsigned int VBO, cubeVAO;
	glGenVertexArrays(1, &cubeVAO);
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindVertexArray(cubeVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	// second, configure the light's VAO (VBO stays the same; the vertices are the same for the light object which is also a 3D cube)
	unsigned int lightVAO;
	glGenVertexArrays(1, &lightVAO);
	glBindVertexArray(lightVAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	// note that we update the lamp's position attribute's stride to reflect the updated buffer data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	//CreateMesh(&obj1, vertices);

	//sb_push(scene_meshes, obj1);
	//Mesh object 2
	/* mesh obj2 = MESH_OBJ_INIT;
	CreateMesh(&obj2, cube_vertices, cube_indices, cube_normals, cube_tex_coords, cube_tex_indices,  24, 36, 6, 8, 6);
	sb_push(scene_meshes, obj2); */

	shader *scene_shaders = NULL;
	//Shaders
	shader sh1 = SHADER_OBJ_INIT;
	CreateShader(&sh1, "shaders/tex_phong_vs.glsl", "shaders/tex_phong_fs.glsl");
	sb_push(scene_shaders, sh1);
	//Light shader
	shader sh2 = SHADER_OBJ_INIT;
	CreateShader(&sh2, "shaders/light_vs.glsl", "shaders/light_fs.glsl");
	sb_push(scene_shaders, sh2);

	unsigned int diffuseMap = loadTexture("assets/container2.png");

	glUseProgram(scene_shaders[0].shaderID);
	glUniform1i(glGetUniformLocation(scene_shaders[0].shaderID, "texture.diffuse"), 0);

	// Loop until window closed
	while (!glfwWindowShouldClose(mainWindow))
	{

		// Get + Handle user input events
		glfwPollEvents();
		glfwGetCursorPos(mainWindow, &xpos, &ypos);
		//Delta time
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		//Light rotation
		// lightPos[0] = cos(glfwGetTime()) * 1.5f;
		// lightPos[2] = sin(glfwGetTime()) * 1.5f;

		processInput(mainWindow, deltaTime, cameraPos, cameraFront, cameraUp);

		// Clear window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(scene_shaders[0].shaderID);
		//Model matrix
		mat4x4 model;
		mat4x4_identity(model);
		//mat4x4_translate(model, 0.0f, -1.0f, 0.0f);
		mat4x4_scale_aniso(model, model, 0.5f, 0.5f, 0.5f);
		// camera/view transformation
		mat4x4 view;
		vec3 cameraNewPos;
		vec3_add(cameraNewPos, cameraPos, cameraFront);
		mat4x4_look_at(view, cameraPos, cameraNewPos, cameraUp);
		//Projection matrix
		mat4x4 projection;
		mat4x4_identity(projection);
		mat4x4_perspective(projection, fov, (float)WIDTH / (float)HEIGHT, 0.1f, 100.0f);

		vec3 cubeMaterialSpecular = {0.5f, 0.5f, 0.5f};
		GLfloat cubeMaterialShininess = 64.0f;
		vec3 cubeLightAmbient = {0.2f, 0.2f, 0.2f};
		vec3 cubeLightDiffuse = {0.5f, 0.5f, 0.5f};
		vec3 cubeLightSpecular = COL_WHITE;

		//Dynamic light
		/* vec3 dynamicLight = {sin(glfwGetTime() * 2.0f), sin(glfwGetTime() * 0.7f), sin(glfwGetTime() * 1.3f)};
		vec3 cubeLightDiffuse;
		vec3_scale(cubeLightDiffuse, dynamicLight, 0.5f);		
		vec3 cubeLightAmbient;
		vec3_scale(cubeLightAmbient, cubeLightDiffuse, 0.2f); */

		TexPhong cubeMat = {&cubeMaterialSpecular, cubeMaterialShininess, &cubeLightAmbient, &cubeLightDiffuse, &cubeLightSpecular};

		SetTexPhongMaterial(&scene_shaders[0], &cubeMat, &lightPos);
		glUniform3fv(scene_shaders[0].viewPos, 1, &cameraNewPos[0]);
		glUniformMatrix4fv(scene_shaders[0].uniformProjection, 1, GL_FALSE, projection[0]);
		glUniformMatrix4fv(scene_shaders[0].uniformView, 1, GL_FALSE, view[0]);
		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, diffuseMap);

		//RenderMesh(&scene_meshes[0]);
		glBindVertexArray(cubeVAO);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glUseProgram(scene_shaders[1].shaderID);

		mat4x4_identity(model);
		mat4x4_translate(model, lightPos[0], lightPos[1], lightPos[2]);
		mat4x4_scale_aniso(model, model, 0.1f, 0.1f, 0.1f);

		vec3 lightMatAmbient = COL_WHITE;
		vec3 lightMatDiffuse = COL_WHITE;
		vec3 lightMatSpecular = COL_WHITE;
		GLfloat lightlMatShininess = 32.0f;
		vec3 lightAmbient = COL_WHITE;
		vec3 lightDiffuse = COL_WHITE;
		vec3 lightSpecular = COL_WHITE;

		Phong lightMat = {&lightMatAmbient, &lightMatDiffuse, &lightMatSpecular, lightlMatShininess, &lightAmbient, &lightDiffuse, &lightSpecular};

		SetPhongMaterial(&scene_shaders[1], &lightMat, &lightPos);
		glUniform3fv(scene_shaders[1].viewPos, 1, &cameraNewPos[0]);
		glUniformMatrix4fv(scene_shaders[1].uniformModel, 1, GL_FALSE, model[0]);
		glUniformMatrix4fv(scene_shaders[1].uniformView, 1, GL_FALSE, view[0]);
		glUniformMatrix4fv(scene_shaders[1].uniformProjection, 1, GL_FALSE, projection[0]);
		//RenderMesh(&scene_meshes[1]);
		glBindVertexArray(lightVAO);
		glDrawArrays(GL_TRIANGLES, 0, 36);

		glUseProgram(0);
		glBindVertexArray(0);
		glfwSwapBuffers(mainWindow);
	}
	//sb_free(scene_meshes);
	sb_free(scene_shaders);

	glDeleteVertexArrays(1, &cubeVAO);
    glDeleteVertexArrays(1, &lightVAO);
    glDeleteBuffers(1, &VBO);

	glfwTerminate();
	return 0;
}

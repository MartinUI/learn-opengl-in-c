#include <stdio.h>
#include <stdbool.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "shader.h"

static bool ParseFileIntoStr(const char *file_name, char *shader_str, int max_len)
{
	FILE *file = fopen(file_name, "r");
	if (!file)
	{
		printf("ERROR: opening file for reading: %s\n", file_name);
		return false;
	}
	size_t cnt = fread(shader_str, 1, max_len - 1, file);
	if ((int)cnt >= max_len - 1)
	{
		printf("WARNING: file %s too big - truncated.\n", file_name);
	}
	if (ferror(file))
	{
		printf("ERROR: reading shader file %s\n", file_name);
		fclose(file);
		return false;
	}
	// append \0 to end of file string
	shader_str[cnt] = 0;
	fclose(file);
	return true;
}

static void AddShader(GLuint theProgram, char *shaderCode, GLenum shaderType)
{

	GLuint theShader = glCreateShader(shaderType);

	ParseFileIntoStr(shaderCode, shader_buf, 1024 * 256);

	shader_source_pointer = (const GLchar *)shader_buf;

	glShaderSource(theShader, 1, &shader_source_pointer, NULL);
	glCompileShader(theShader);

	GLint result = 0;
	GLchar eLog[1024] = {0};

	glGetShaderiv(theShader, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(theShader, 1024, NULL, eLog);
		printf("Error compiling the %d shader: '%s'\n", shaderType, eLog);
		return;
	}

	glAttachShader(theProgram, theShader);
}

void CreateShader(shader *shader_object, char *vertexShaderCode, char *fragmentShaderCode)
{
	shader_object->shaderID = glCreateProgram();

	if (!shader_object->shaderID)
	{
		printf("Failed to create shader\n");
		return;
	}

	AddShader(shader_object->shaderID, vertexShaderCode, GL_VERTEX_SHADER);
	AddShader(shader_object->shaderID, fragmentShaderCode, GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar eLog[1024] = {0};

	glLinkProgram(shader_object->shaderID);
	glGetProgramiv(shader_object->shaderID, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shader_object->shaderID, sizeof(eLog), NULL, eLog);
		printf("Error linking program: '%s'\n", eLog);
		return;
	}

	glValidateProgram(shader_object->shaderID);
	glGetProgramiv(shader_object->shaderID, GL_VALIDATE_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shader_object->shaderID, sizeof(eLog), NULL, eLog);
		printf("Error validating program: '%s'\n", eLog);
		return;
	}
	shader_object->uniformModel = glGetUniformLocation(shader_object->shaderID, "model");
	shader_object->uniformView = glGetUniformLocation(shader_object->shaderID, "view");
	shader_object->uniformProjection = glGetUniformLocation(shader_object->shaderID, "projection");
	shader_object->viewPos = glGetUniformLocation(shader_object->shaderID, "viewPos");	
}
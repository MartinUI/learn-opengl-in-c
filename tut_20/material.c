#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "material.h"
#include "shader.h"


void SetPhongMaterial(shader *shader_object, Phong *material_object, vec3 *lightPosition){

	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "material.ambient"), 1, material_object->materialAmbient[0]); 
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "material.diffuse"), 1, material_object->materialDiffuse[0]); 
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "material.specular"), 1, material_object->materialSpecular[0]);
	glUniform1f(glGetUniformLocation(shader_object->shaderID, "material.shininess"), material_object->materialShininess);	
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.ambient"), 1, material_object->lightAmbient[0]); 
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.diffuse"), 1, material_object->lihtDiffuse[0]); 
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.specular"), 1, material_object->lightSpecular[0]);
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.position"), 1, lightPosition[0]);	
}

void SetTexPhongMaterial(shader *shader_object, TexPhong *material_object, vec3 *lightPosition){

	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "tex.specular"), 1, material_object->materialSpecular[0]);
	glUniform1f(glGetUniformLocation(shader_object->shaderID, "tex.shininess"), material_object->materialShininess);	
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.ambient"), 1, material_object->lightAmbient[0]); 
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.diffuse"), 1, material_object->lihtDiffuse[0]); 
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.specular"), 1, material_object->lightSpecular[0]);
	glUniform3fv(glGetUniformLocation(shader_object->shaderID, "light.position"), 1, lightPosition[0]);	
}
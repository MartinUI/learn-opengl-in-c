#ifndef WINDOW_H
#define WINDOW_H

#include <GLFW/glfw3.h>


GLint bufferWidth, bufferHeight;

GLFWwindow* CreateWindow(GLint windowWidth, GLint windowHeight, char *windowName);

#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cglm/cglm.h>

#include "window.h"		
#include "mesh.h"
#include "shader.h"

#include "stretchy_buffer.h"

// Window dimensions
//const GLint WIDTH = 800, HEIGHT = 600;
//const float toRoradians = 3.14159265f / 180.0f;

unsigned int indices[] = {
	0, 3, 1,
	1, 3, 2,
	2, 3, 0,
	0, 1, 2};

GLfloat vertices[] = {
	-1.0f, -1.0f, 0.0f,
	0.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 0.0f,
	0.0f, 1.0f, 0.0f};

int main()
{	
	//Window
	GLFWwindow *main_window = NULL;	
	main_window = CreateWindow(800,600,"Test window");

	//Meshes
	mesh *scene_meshes = NULL;
	//Mesh object 1
	mesh obj1 = MESH_OBJ_INIT;
	CreateMesh(&obj1, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj1);	
	//Mesh object 2
	mesh obj2 = MESH_OBJ_INIT;
	CreateMesh(&obj2, vertices, indices, 12, 12);
	sb_push(scene_meshes, obj2);
	//Shaders
	shader *scene_shaders = NULL;
	
	shader sh1 = SHADER_OBJ_INIT;
	CreateShader(&sh1, "test_vs.glsl","test_fs.glsl");	
	sb_push(scene_shaders, sh1);
	
	//Projection matrix
	mat4 projection = GLM_MAT4_IDENTITY_INIT;
	glm_perspective(45.0f, ((GLfloat)bufferWidth / (GLfloat)bufferHeight), 0.1f, 100.0f, projection);

	// Loop until window closed
	while (!glfwWindowShouldClose(main_window))
	{
		// Get + Handle user input events
		glfwPollEvents();
		// Clear window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(scene_shaders[0].shaderID);
		//Model matrix
		mat4 model = GLM_MAT4_IDENTITY_INIT;
		glm_translate(model, (vec3){0.0f, 0.0f, -2.5f});
		//glm_rotate(model,curAngle * toRoradians, (vec3){0.0f,1.0f,0.0f});
		glm_scale(model, (vec3){0.4f, 0.4f, 1.0f});
		//Attach model matrix
		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);
		//Attach projection matrix
		glUniformMatrix4fv(scene_shaders[0].uniformProjection, 1, GL_FALSE, projection[0]);
		//Render mesh
		RenderMesh(&scene_meshes[0]);
		glm_mat4_identity(model);	

		glm_translate(model, (vec3){0.0f, 1.0f, -2.5f});
		glm_scale(model, (vec3){0.4f, 0.4f, 1.0f});
		glUniformMatrix4fv(scene_shaders[0].uniformModel, 1, GL_FALSE, model[0]);
		RenderMesh(&scene_meshes[1]);

		glUseProgram(0);
		glfwSwapBuffers(main_window);
	}
	sb_free(scene_meshes);
	sb_free(scene_shaders);
	
	return 0;
}

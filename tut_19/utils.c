#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "utils.h"
#include "linmath.h"
#include "stb_image.h"

void processInput(GLFWwindow *window, float deltaTime, vec3 cameraPos, vec3 cameraFront, vec3 cameraUp)	
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	float cameraSpeed = 2.5f * deltaTime;

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		vec3 cameraPosMul;
		vec3_scale(cameraPosMul,cameraPos,cameraSpeed);
		vec3_add(cameraPos,cameraPos,cameraPosMul);
	}		
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		vec3 cameraPosMul;
		vec3_scale(cameraPosMul,cameraPos,cameraSpeed);
		vec3_sub(cameraPos,cameraPos,cameraPosMul);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		vec3 cameraPosMul;
		vec3 cameraCross;
		vec3_mul_cross(cameraCross,cameraFront,cameraUp);
		vec3_norm(cameraCross,cameraCross);
		vec3_scale(cameraPosMul,cameraCross,cameraSpeed);
		vec3_sub(cameraPos,cameraPos,cameraPosMul);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
		vec3 cameraPosMul;
		vec3 cameraCross;
		vec3_mul_cross(cameraCross,cameraFront,cameraUp);
		vec3_norm(cameraCross,cameraCross);
		vec3_scale(cameraPosMul,cameraCross,cameraSpeed);
		vec3_add(cameraPos,cameraPos,cameraPosMul);
	}
}
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    xoffset = xpos - lastX;
    yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
    lastX = xpos;
    lastY = ypos;
    
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;
    pitch += yoffset;

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    vec3 front;

    front[0] = cos(yaw) * cos(pitch);
    front[1] = sin(pitch);
    front[2] = sin(yaw) * cos(pitch);
    vec3_norm(cameraFront,front);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    if (fov >= 1.0f && fov <= 45.0f)
        fov -= yoffset;
    if (fov <= 1.0f)
        fov = 1.0f;
    if (fov >= 45.0f)
        fov = 45.0f;
}
// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
unsigned int loadTexture(char const * path)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);
    
    int width, height, nrComponents;
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        printf("Texture failed to load at path: %s\n", path);
        stbi_image_free(data);
    }

    return textureID;
}
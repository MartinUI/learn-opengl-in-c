#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "mesh.h"

void CreateMesh(mesh *mesh_object, GLfloat *vertices, unsigned int *indeces, unsigned int *normals, unsigned int *text_coords, unsigned int numOfVerteces, unsigned int numOfIndecies, unsigned int numOfTexCoords){

    mesh_object->indexCount = numOfIndecies;

    glGenVertexArrays(1, &(mesh_object->VAO));
	glBindVertexArray(mesh_object->VAO);
				
		//Vertex buffer arrray
		glGenBuffers(1, &(mesh_object->VBO));

		glBindBuffer(GL_ARRAY_BUFFER, mesh_object->VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * numOfVerteces + sizeof(text_coords[0]) * numOfTexCoords, NULL, GL_STATIC_DRAW);
		
		glBufferSubData(GL_ARRAY_BUFFER, 0,  sizeof(vertices[0]) * numOfVerteces, vertices);
		glBufferSubData(GL_ARRAY_BUFFER,  sizeof(vertices[0]) * numOfVerteces, sizeof(text_coords[0]) * numOfTexCoords, GL_STATIC_DRAW);
		
		
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 0, sizeof(vertices[0]) * numOfVerteces);

		//Vertex texture coorinates arrray
		//glGenBuffers(1, &(mesh_object->TBO));
		//glBindBuffer(GL_ARRAY_BUFFER, mesh_object->TBO);		

		//Element array buffer
		glGenBuffers(1, &(mesh_object->IBO));
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh_object->IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indeces[0]) * numOfIndecies, indeces, GL_STATIC_DRAW);	

		/* //Vertex buf normals arrray
		glGenBuffers(1, &(mesh_object->NBO));
		glBindBuffer(GL_ARRAY_BUFFER, mesh_object->NBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(normals[0]) * numOfNormals, normals, GL_STATIC_DRAW); */

		 


	
	glEnableVertexAttribArray(0);
			
	//Unbind vertex buffer array
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Unvind element array
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//Unbind vertex array
	glBindVertexArray(0);
}

void RenderMesh(mesh *mesh_object){ 
	//Bind to vertex array
	glBindVertexArray(mesh_object->VAO);
		//Bind to element buffer array
	    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,mesh_object->IBO);		
			//Draw element
			glDrawElements(GL_TRIANGLES, mesh_object->indexCount, GL_UNSIGNED_INT, 0);
		//Unbind element buffer array
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	//Unbind vertex array
	glBindVertexArray(0);
}

void ClearMesh(mesh *mesh_object){    
    if (mesh_object->IBO != 0) {
        glDeleteBuffers(1, &(mesh_object->IBO));
        mesh_object->IBO = 0;
    }
    if (mesh_object->VBO != 0) {
        glDeleteBuffers(1, &(mesh_object->VBO));
        mesh_object->VBO = 0;
    }
    if (mesh_object->IBO != 0) {
        glDeleteVertexArrays(1, &(mesh_object->IBO));
        mesh_object->VAO = 0;
    }
    mesh_object->indexCount = 0; 
}

#ifndef MESH_H
#define MESH_H

#include <GLFW/glfw3.h>

#define MESH_OBJ_INIT {0, 0, 0, 0, 0, 0}

typedef struct {
    GLuint VBO;
    GLuint VAO;
    GLuint IBO;
    GLuint NBO;
    GLuint TBO;
    GLsizei indexCount;
} mesh;

void CreateMesh(mesh *mesh_object, GLfloat *vertices, unsigned int *indeces, unsigned int *normals, unsigned int *text_coords, unsigned int numOfVerteces, unsigned int numOfIndecies, unsigned int numOfTexCoords);
void RenderMesh(mesh *mesh_object);
void ClearMesh();

#endif
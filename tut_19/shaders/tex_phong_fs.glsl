#version 330

struct Texture {
    int shininess;
    sampler2D diffuse;
    vec3 specular;    
};

struct Light {
    vec3 position;  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Texture tex;
uniform Light light;    
uniform vec3 viewPos;

in vec2 texCoord;
in vec3 normal;
in vec3 fragPos; 

out vec4 colour;

void main()
{

    vec3 fill = vec3(0.2);
    vec3 ambient = light.ambient * texture(tex.diffuse, texCoord).rgb;

    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(light.position - fragPos);  

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(tex.diffuse, texCoord).rgb;

    vec3 viewDir = normalize(viewPos - fragPos);
    vec3 reflectDir = reflect(-lightDir, norm);



    float spec = pow(max(dot(viewDir, reflectDir), 0.0), tex.shininess);
    vec3 specular = light.specular * (spec * tex.specular);     

    vec3 result = ambient + diffuse + specular; 
    
    colour = vec4(result, 1.0);
}